//
//  HomeTableViewController.swift
//  TestingConstraint
//
//  Created by Reaksmey on 5/2/23.
//

import UIKit
import Inject

class HomeViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    override var navigationBackgroundColor: UIColor {
        Colors.primary200
    }

    override var navigationTitleFont: UIFont? {
        UIFont.systemFont(ofSize: 18, weight: .bold)
    }

    override var navigationTitle: String? {
        "PROMOTION"
    }
    override var navigationTitleColor: UIColor? {
        Colors.whiteFFF
    }
    
    override func initializeUI() {
        super.initializeUI()
    }
    
    let homeTableView: UITableView = {
        let tableview = UITableView()
        tableview.translatesAutoresizingMaskIntoConstraints = false
        return tableview
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(homeTableView)
        homeTableView.delegate = self
        homeTableView.dataSource = self
        homeTableView.anchor(left: view.leftAnchor,
                             right: view.rightAnchor,
                             top: view.topAnchor,
                             bottom: view.bottomAnchor,
                             padding: .zero)
        homeTableView.register(HomeTableViewCell.self, forCellReuseIdentifier: HomeTableViewCell.identifier)
    }
    
    // MARK: - Table view data source
      
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: HomeTableViewCell.identifier,
                                                 for: indexPath) as! HomeTableViewCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }

}
