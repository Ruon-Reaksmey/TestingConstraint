//
//  RootViewControllerHelper.swift
//  TestingConstraint
//
//  Created by Reaksmey on 5/2/23.
//

import UIKit


enum RootScreenType {
    case rootTabbar(_ index: Int)
}

class RootViewControllerHelper: NSObject {
    
    public var window: UIWindow?
    
    public static var shared = RootViewControllerHelper()
    
    private override init() {
        
    }
    
    public func backHomeScreen() {
        let root = window?.rootViewController
        root?.dismiss(animated: false, completion: {
            let tabbar = (root as? UITabBarController)
            let homeIndex = RootTabBarItems.home.rawValue
            tabbar?
                .viewControllers?[tabbar?.selectedIndex ?? homeIndex]
                .rootViewController?
                .popToRootViewController(animated: false)
            tabbar?.selectedIndex = homeIndex
        })
    }
    
    public func switchScreen(to screen: RootScreenType = .rootTabbar(RootTabBarItems.home.rawValue)) {
        switch screen {
        case .rootTabbar(let index):
            let tabBar = RootTabBarController()
            
            if let viewController = tabBar.viewControllers?[index] {
                let shouldSelect = tabBar.tabBarController(tabBar, shouldSelect: viewController)
                if shouldSelect {
                    tabBar.selectedIndex = index
                }
            }
            window?.rootViewController = tabBar
            window?.makeKeyAndVisible()
        }
    }
    
}

