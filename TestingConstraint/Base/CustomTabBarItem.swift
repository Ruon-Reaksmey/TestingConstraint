//
//  CustomTabBarItem.swift
//  TestingConstraint
//
//  Created by Reaksmey on 5/2/23.
//

import UIKit

struct CustomTabBarItem {
    let title: String
    let icon: String
    let iconSelected: String
    let viewController: UIViewController
    let systemTab: UITabBarItem?
    
    init(tab: UITabBarItem, viewController: UIViewController) {
        self.systemTab = tab
        self.viewController = viewController
        self.title = tab.title ?? ""
        self.icon = ""
        self.iconSelected = ""
    }
    
    init(title: String,
         icon: String,
         selectedIcon: String,
         viewController: UIViewController,
         systemTab: UITabBarItem? = nil) {
        self.title          = title
        self.icon           = icon
        self.viewController = viewController
        self.systemTab      = systemTab
        self.iconSelected   = selectedIcon
    }
    
    
    public var getTabIcon: UIImage? {
        return UIImage(named: self.icon)?.withRenderingMode(.alwaysTemplate)
    }
    
    public func getTabBarItem(with renderingMode: UIImage.RenderingMode, customSize: CGSize?) -> UITabBarItem {
        let image = customSize.isNull ? UIImage(named: icon) : UIImage(named: icon)?.reSizeImage(reSize: customSize!)
        let selectedImage = customSize.isNull ? UIImage(named: iconSelected) : UIImage(named: iconSelected)?.reSizeImage(reSize: customSize!)
        let tab = UITabBarItem(title: title,
                               image: image?.withRenderingMode(renderingMode),
                               selectedImage: selectedImage?.withRenderingMode(renderingMode))
        return systemTab ?? tab
    }
    
}
