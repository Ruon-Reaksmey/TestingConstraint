//
//  RootTabBarController.swift
//  TestingConstraint
//
//  Created by Reaksmey on 5/2/23.
//

import UIKit
import Inject

enum RootTabBarItems: Int, CaseIterable {
    case home = 0
    case promotion
    case notification
    case contactus
    
    public var tab: CustomTabBarItem {
        switch self {
        case .home:
            return CustomTabBarItem(title: "",
                                    icon: RootIconNames.iconHome.rawValue,
                                    selectedIcon: RootIconNames.iconHome.rawValue,
                                    viewController: ViewController()
            )
            
        case .promotion:
            return CustomTabBarItem(title: "",
                                    icon: RootIconNames.iconPromotion.rawValue,
                                    selectedIcon: RootIconNames.iconPromotion.rawValue,
                                    viewController: HomeViewController())
        case .notification:
            return CustomTabBarItem(title: "",
                                    icon: RootIconNames.iconNotification.rawValue,
                                    selectedIcon: RootIconNames.iconNotification.rawValue,
                                    viewController: UIViewController())
        case .contactus:
            return CustomTabBarItem(title: "",
                                    icon: RootIconNames.iconContactUs.rawValue,
                                    selectedIcon: RootIconNames.iconContactUs.rawValue,
                                    viewController: UIViewController())
        }
    }
}

final class RootTabBarController: BaseTabBarController {
    
    override var selectedItemTintColor: UIColor {
        Colors.secondary500
    }
    
    override var unselectedItemTintColor: UIColor {
        Colors.tabBarUnselected
    }
    
    override var tabBarIconRenderingMode: UIImage.RenderingMode {
        .alwaysTemplate
    }
    
    override var customTabBarItemSize: CGSize? {
        CGSize(width: 24.0, height: 24.0)
    }
    
    override var tabBars: [CustomTabBarItem] {
        RootTabBarItems.allCases.map({ $0.tab })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
