//
//  ChainableUIKit.swift
//  TestingConstraint
//
//  Created by Reaksmey on 6/2/23.
//

import Foundation

public struct ChainableWrapper<Wrapped> {
    
    public let wrapped: Wrapped
    
    public init(wrapped: Wrapped) {
        self.wrapped = wrapped
    }
    
    @discardableResult
    public func custom(_ handler: (Self) -> Self) -> Self {
        handler(self)
    }
    
}

public protocol ChainableType {}

public extension ChainableType {
    
    var chainable: ChainableWrapper<Self> { ChainableWrapper(wrapped: self) }
    
}
