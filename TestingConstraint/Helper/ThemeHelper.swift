//
//  ThemeHelper.swift
//  TestingConstraint
//
//  Created by Reaksmey on 5/2/23.
//

import UIKit


public enum ThemeMode: Int {
    case dark = 0
    case light
    case system
    
    var userInterfaceStyle: UIUserInterfaceStyle {
        switch self {
        case .dark:
            return .dark
        case .light:
            return .light
        case .system:
            return .unspecified
        }
    }
}

final class ThemeHelper: NSObject {
    
    private override init() {}
    public static var shared = ThemeHelper()
    
    var currentTheme: ThemeMode {
        get {
            ThemeMode(rawValue: UserDefaults
                .standard
                .integer(forKey: GeneralConstant.userInterfaceStyle)) ?? .light
        }
        set {
            UserDefaults
                .standard
                .set(newValue.rawValue, forKey: GeneralConstant.userInterfaceStyle)
            UIApplication.shared.override(newValue.userInterfaceStyle)
        }
    }
}
