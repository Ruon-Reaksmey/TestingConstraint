//
//  NSObject+Ex.swift
//  TestingConstraint
//
//  Created by Reaksmey on 6/2/23.
//

import Foundation


extension NSObject {
    func delay(_ second: CGFloat, _ execute: @escaping VoidClosure) {
        DispatchQueue.main.asyncAfter(deadline: .now() + second, execute: execute)
    }
}
