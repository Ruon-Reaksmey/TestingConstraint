//
//  GeneralConstant.swift
//  TestingConstraint
//
//  Created by Reaksmey on 5/2/23.
//

import UIKit

struct GeneralConstant {
    
    enum InfoDictionary: String {
        case appBaseURL = "app_base_url"
        case appSocketBaseURL = "socket_base_url"
    }
    
    enum RequestTimeOut: Double {
        case short = 10
        case normal = 20
        case long = 30
    }
    public static let appFallbackURL = "https://apps.apple.com/us/app/ut-swap/id1641053244"
    public static let maxNotificationBadge = 9
    public static let defualtNumbericValue = "0"
    public static let currencyDefaultValue = Double.zero.toDollarFormat
    public static let maxDecimalPlace = 2
    public static let maxDigit = 15
    static let bottomButtonMargin: CGFloat = 14.0
    static let requestTimeOut: Double = 30
    static let socketRequestTimeOut: Double = 5
    static let unAuthenticatedMessage = "Please sign in"
    static let notAvailablePlaceholder = "N/A"
    static let underscore = "_"
    static let dummyView = UIView(frame: CGRect(x: 0, y: 0, width: CGFloat.leastNonzeroMagnitude, height: CGFloat.leastNonzeroMagnitude))
    static let internetConnectionChanage = "internetConnectionChanage"
    static let userInterfaceStyle = "userInterfaceStyle"
    static let size16 = CGSize(width: 16.0, height: 16.0)
    static let size18 = CGSize(width: 18.0, height: 18.0)
    static let size24 = CGSize(width: 24.0, height: 24.0)
    static let size30 = CGSize(width: 30.0, height: 30.0)
    static let optDurationInSecond = 120.0
    
    struct Toast {
        static let generalToastHeight: CGFloat = 56.0
        static let toastHeight: CGFloat = 38.0
        static let toastLimitedDuration = 5.0
        static let toastUlimitedDuration = Double.infinity
    }
    
    struct Auth {
        static let inputPINCount = 6
    }
}
