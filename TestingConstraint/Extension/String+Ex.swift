//
//  String+Ex.swift
//  TestingConstraint
//
//  Created by Reaksmey on 5/2/23.
//

import UIKit

extension String {
    
    public var toDictionary: [String: Any]? {
        if let data = data(using: .utf8) {
                do {
                    return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                } catch {
                    print(error.localizedDescription)
                }
            }
            return nil
    }
    
    public func toPercentage(includeZero: Bool = false) -> String {
        self.toDouble.isNegative ? "\(self.toNumberFormat)%" : (toDouble.isZero ? (includeZero ? "+\(self.toNumberFormat)%" : "\(self.toNumberFormat)%") : "+\(self.toNumberFormat)%")
    }
    
    public var toUTFormat: String {
        toDouble.toUTFormat
    }
    
    public var toDollarFormat: String {
        toDouble.toDollarFormat
    }
    
    public var toNumberFormatWithoutRounding: String {
        toDouble.toNumberFormatWithoutRounding
    }
    
    public var toNumberFormat: String {
        toDouble.toNumberFormat
    }
    
    var dateFromString: Date? {
        let format = DateFormatter()
        format.dateFormat = "dd-MM-yyyy"
        format.locale = .init(identifier: "en_US")
        return format.date(from: self)
    }
    
    var fullDateFromString: Date? {
        let format = DateFormatter()
        format.dateFormat = "d-M-Y H-m-s"
        format.locale = .init(identifier: "en_US")
        return format.date(from: self)
    }
    
    var imageFromBase64: UIImage? {
        guard let imageData = Data(base64Encoded: self, options: .ignoreUnknownCharacters) else {
            return nil
        }
        return UIImage(data: imageData)
    }
    
    public var formatedPhoneNumber: String {
        applyPatternOnNumbers(pattern: "### ### ####", replacementCharacter: "#")
    }
    
    public var formatedNumber3degitWithspace: String {
        applyPatternOnNumbers(pattern: "### ### ### ### ###", replacementCharacter: "#")
    }
    
    public var isNotEmpty: Bool {
        !isEmpty
    }
    
    var imageFromString: UIImage? {
        return UIImage(named: self)
    }
    
    var withColon: String {
        return self + ": "
    }
    
    func applyPatternOnNumbers(pattern: String, replacementCharacter: Character) -> String {
        var pureNumber = self.replacingOccurrences( of: "[^0-9]", with: "", options: .regularExpression)
        for index in 0 ..< pattern.count {
            guard index < pureNumber.count else { return pureNumber }
            let stringIndex = String.Index(utf16Offset: index, in: pattern)
            let patternCharacter = pattern[stringIndex]
            guard patternCharacter != replacementCharacter else { continue }
            pureNumber.insert(patternCharacter, at: stringIndex)
        }
        return pureNumber
    }
    
    var isEmptyOrWhitespac: Bool {
        if self.isEmpty { return true }
        return self.trimmingCharacters(in: .whitespacesAndNewlines) == ""
    }
    
    var isNotEmptyOrWhiteSpace: Bool {
        if self.isEmpty { return false }
        return self.trimmingCharacters(in: .whitespacesAndNewlines) != ""
    }
    
    func addWhiteSpace() -> String  { return self + " " }
    func addATap() -> String  { return self + "   " }
    
    var containLetter: Bool {
        let letters = NSCharacterSet.letters
        let range = self.rangeOfCharacter(from: letters)
        if let _ = range {
            return true
        } else {
            return false
        }
    }
    
    var condensedWhitespace: String {
        let components = self.components(separatedBy: NSCharacterSet.whitespacesAndNewlines)
        return components.filter { !$0.isEmpty }.joined(separator: " ")
    }
    
    func containsNumbers() -> Bool
    {
        let numberRegEx  = ".*[0-9]+.*"
        let testCase = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
        return testCase.evaluate(with: self)
    }
    
    var removeLeadingAndtrailingSpaces: String {
        return self.trimmingCharacters(in: .whitespaces)
    }
    
    var removeAllSpaces: String {
        return self.replacingOccurrences(of: " ", with: "")
    }
    
    var removeAllCommas: String {
        return self.replacingOccurrences(of: ",", with: "")
    }
    
    var containSpecialCharacter: Bool {
        let characterset = NSCharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789\(" ")")
        if self.rangeOfCharacter(from: characterset.inverted) != nil {
            return true
        }
        return false
    }
    
    var roundedWithAbbreviations: String {
        let number = Double(self)
        let thousand = number! / 1000
        let million = number! / 1000000
        if million >= 1.0 {
            return "\(round(million*10)/10)M"
        }
        else if thousand >= 1.0 {
            return "\(round(thousand*10)/10)K"
        }
        else {
            return "\(self)"
        }
    }
    
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    
    var toDouble: Double {
        get {
            return Double.init(self.removeAllSpaces.removeAllCommas) ?? 0.0
        }
    }
    
    var toInt: Int {
        get {
            return Int.init(self.removeAllSpaces.removeAllCommas) ?? 0
        }
    }
    
    var isNumber: Bool {
        return !isEmpty && rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil
    }
    
    func isEqual(st: String) -> Bool {
        return self.lowercased() == st.lowercased()
    }
    
    func digitsOnly() -> String{
        let stringArray = self.components(separatedBy: NSCharacterSet.decimalDigits.inverted)
        let newString = NSArray(array: stringArray).componentsJoined(by: "")
        return newString == "" ? "" : newString
    }
    
    func toDoubleText() -> String {
        let stringArray = self.components(separatedBy: CharacterSet(charactersIn: "0123456789.,").inverted)
        let newString = NSArray(array: stringArray).componentsJoined(by: "")
        return newString == "" ? "" : newString
    }
    
    func onlyFirstCharCapitalized() -> String {
        guard !isEmpty else { return self }
        let lowerCasedString = self.lowercased()
        return lowerCasedString.replacingCharacters(in: lowerCasedString.startIndex...lowerCasedString.startIndex, with: String(lowerCasedString[lowerCasedString.startIndex]).uppercased())
    }
    
    var isValidURL: Bool {
        guard let url = URL(string: self) else { return false }
        return UIApplication.shared.canOpenURL(url)
    }
    
    func makePhoneCall() {
        if let url = URL(string: "tel://\(self)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    func toNSNumber()-> NSNumber {
        return NumberFormatter().number(from: self) ?? 0
    }
    
    func toNSNumberArray()-> [NSNumber] {
        self.components(separatedBy: ",").map{$0.toNSNumber()}
    }
    
    func convertToDictionary() -> [String: Any]? {
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch let error {
                print("convertToDic() -> ", error)
            }
        }
        return nil
    }
}

extension String {
    
    mutating func replaceSubString(startAt: Int, offsetBy: Int, with newString: String) {
        let startIndex = self.index(self.startIndex, offsetBy: startAt)
        let endIndex = self.index(startIndex, offsetBy: offsetBy)
        self.replaceSubrange(startIndex..<endIndex, with: newString)
    }
    
    
    func slice(from: String, to: String) -> String? {
        return (range(of: from)?.upperBound).flatMap { substringFrom in
            (range(of: to, range: substringFrom..<endIndex)?.lowerBound).map { substringTo in
                String(self[substringFrom..<substringTo])
            }
        }
    }
    
    func index(from: Int) -> Index {
        return self.index(startIndex, offsetBy: from)
    }
    
    func substring(from: Int) -> String {
        let fromIndex = index(from: from)
        return String(self[fromIndex...])
    }
    
    func substring(to: Int) -> String {
        let toIndex = index(from: to)
        return String(self[..<toIndex])
    }
    
    func substring(with r: Range<Int>) -> String {
        let startIndex = index(from: r.lowerBound)
        let endIndex = index(from: r.upperBound)
        return String(self[startIndex..<endIndex])
    }
    
    func encodeUrl() -> String? {
        return self.addingPercentEncoding( withAllowedCharacters: NSCharacterSet.urlQueryAllowed)
    }
    
    func decodeUrl() -> String? {
        return self.removingPercentEncoding
    }
    
    func trim() -> String {
        return self.trimmingCharacters(in: NSCharacterSet.whitespaces)
    }
    
    func contains(find: String) -> Bool{
        return self.range(of: find) != nil
    }
    func containsIgnoringCase(find: String) -> Bool{
        return self.range(of: find, options: .caseInsensitive) != nil
    }
    
    func index(at position: Int, from start: Index? = nil) -> Index? {
        let startingIndex = start ?? startIndex
        return index(startingIndex, offsetBy: position, limitedBy: endIndex)
    }
    
    func character(at position: Int) -> Character? {
        guard position >= 0, let indexPosition = index(at: position) else {
            return nil
        }
        return self[indexPosition]
    }
}


