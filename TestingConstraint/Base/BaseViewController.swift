//
//  BaseViewController.swift
//  TestingConstraint
//
//  Created by Reaksmey on 5/2/23.
//

import UIKit


@objc protocol CommonViewControllerActions {
    @objc func initializeUI()
}


class BaseViewController : UIViewController{
    
    public var didGoBack: VoidClosure?
    
    var currentKeyboardHeight: CGFloat = .zero
    var observeKeyboardShow: NSObjectProtocol?
    var observeKeyboardHide: NSObjectProtocol?

    public var contentLoadingView: UIView?
    
    public var enableInternetConnectionTracking: Bool {
        true
    }
    
    public var statusBarBackgroundColor: UIColor? {
        nil
    }
    
    public var additionalNavigationMarginTop: CGFloat {
        .zero
    }
    
    public var additionalNavigationMarginBottom: CGFloat {
        .zero
    }
    
    public var statusBarStyle: UIStatusBarStyle = .default {
        didSet {
            (navigationController as? BaseNavigationController)?.statusBarStyle = statusBarStyle
            setNeedsStatusBarAppearanceUpdate()
        }
    }
    
    public var isTranslucent: Bool {
        false
    }
    
    public var hideKeyboardWhenTappedAround: Bool {
        true
    }
    
    public var additionalNavigationMarginLeft: CGFloat {
        .zero
    }
    
    public var additionalNavigationMarginRight: CGFloat {
        .zero
    }
    
    public var enableNavigationSeparateLine: Bool {
        false
    }
    
    public var bottomNavigationSeperateLineColor: UIColor {
        .systemGray5
    }
    
    public var bottomNavigationSeperateLineHeight: CGFloat {
        1.0
    }
    
    public var defaultViewBackgroundColor: UIColor {
        .white
    }
    
    public var navigationTitle: String? {
        nil
    }
    
    public var customTitleView: UIView? {
        nil
    }
    
    public var navigationBackgroundColor: UIColor {
        .white
    }
    
    public var navigationTitleColor: UIColor? {
        nil
    }
    
    public var navigationTitleFont: UIFont? {
        nil
    }
    
    public var navigationTitleAttribute: [NSAttributedString.Key : Any]? {
        nil
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return statusBarStyle
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        prepareNavigation(by: navigationBackgroundColor)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeUI()
    }
    
}

// MARK: -Public Functions
extension BaseViewController {
    
    
    @objc public func prepareNavigation(by navigationBackgroundColor: UIColor) {
        navigationController?.additionalSafeAreaInsets = UIEdgeInsets(top: additionalNavigationMarginTop,
                                                                      left: additionalNavigationMarginLeft,
                                                                      bottom: additionalNavigationMarginBottom,
                                                                      right: additionalNavigationMarginRight)
        let navigationBar = navigationController?.navigationBar
        if #available(iOS 15, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = navigationBackgroundColor
            if enableNavigationSeparateLine {
                appearance.shadowImage = bottomNavigationSeperateLineColor
                    .toImage(height: bottomNavigationSeperateLineHeight)
            } else {
                appearance.shadowColor = .clear
            }
            navigationBar?.standardAppearance   = appearance
            navigationBar?.scrollEdgeAppearance = appearance
        } else {
            navigationBar?.backgroundColor = navigationBackgroundColor
            navigationBar?.barTintColor    = navigationBackgroundColor
            navigationBar?.isTranslucent   = isTranslucent
            
            if enableNavigationSeparateLine {
                navigationBar?.shadowImage = bottomNavigationSeperateLineColor
                    .toImage(height: bottomNavigationSeperateLineHeight)
            } else {
                navigationBar?.shadowImage = UIColor.clear.toImage()
            }
        }
        
        navigationBar?.setValue(!enableNavigationSeparateLine, forKey: "hidesShadow")
        
    }
    
    @objc public func setNavigationTitle(_ title: String?) {
        if #available(iOS 15.0, *) {
            let label = UILabel()
            label.text = title
            navigationTitleFont.unwrap({
                label.font = $0
            })
            navigationTitleColor.unwrap({
                label.textColor = $0
            })
            navigationTitleAttribute.unwrap({
                let attr = NSAttributedString(string: title ?? "", attributes: $0)
                label.attributedText = attr
            })
            navigationItem.titleView = label
        } else {
            navigationItem.title = title
            navigationTitleAttribute.unwrap({
                self.navigationController?.navigationBar.titleTextAttributes = $0
            })
        }
    }
    
    public func setCustomTitleView(_ customTitleView: UIView) {
        self.navigationItem.titleView = customTitleView
    }
}

// MARK: -Private Functions
extension BaseViewController {

}


extension BaseViewController: CommonViewControllerActions  {
    internal func initializeUI() {
        view.backgroundColor = defaultViewBackgroundColor
        
        let bounds = UIScreen.main.bounds
        contentLoadingView = UIView(frame: CGRect(x: 0, y: 0, width: bounds.width, height: bounds.height))
        contentLoadingView?.backgroundColor = .black.withAlphaComponent(0.2)
        
        setNavigationTitle(navigationTitle)
        
        customTitleView.unwrap({
            self.setCustomTitleView($0)
        })
    }
    

}
