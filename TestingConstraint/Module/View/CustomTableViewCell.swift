//
//  CustomTableViewCell.swift
//  TestingConstraint
//
//  Created by Reaksmey on 3/2/23.
//

import Foundation
import UIKit

class CustomTableViewCell: UITableViewCell {
    
    static let identifier = "CustomTableViewCell"
    
    private let myImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "icon_2019")
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(myImageView)
        
        
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    public func configure(text: String, imageName: String) {
        myImageView.image = UIImage(named: imageName)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        myImageView.image =  nil
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        myImageView.translatesAutoresizingMaskIntoConstraints = false
        myImageView.anchor(left: self.contentView.leadingAnchor,
                           centerY: self.contentView.centerYAnchor,
                           width: 70,
                           height: 70,
                           padding: .init(top: 0,
                                          left: 10,
                                          bottom: 0,
                                          right: 0))
    }
}
