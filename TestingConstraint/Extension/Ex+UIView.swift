//
//  Ex+UIView.swift
//  TestingConstraint
//
//  Created by Reaksmey on 5/2/23.
//
import UIKit

extension UIView {
    
    func anchor(left: NSLayoutAnchor<NSLayoutXAxisAnchor>, right: NSLayoutAnchor<NSLayoutXAxisAnchor>,
                top: NSLayoutAnchor<NSLayoutYAxisAnchor>, bottom: NSLayoutAnchor<NSLayoutYAxisAnchor>,
                padding:UIEdgeInsets = .zero) {
        leftAnchor.constraint(equalTo: left, constant: padding.left).isActive = true
        rightAnchor.constraint(equalTo: right, constant: padding.right).isActive = true
        topAnchor.constraint(equalTo: top, constant: padding.top).isActive = true
        bottomAnchor.constraint(equalTo: bottom, constant: padding.bottom).isActive = true
    }
    
    func anchor(left: NSLayoutAnchor<NSLayoutXAxisAnchor>,
                centerY: NSLayoutAnchor<NSLayoutYAxisAnchor>,
                width: CGFloat,
                height: CGFloat,
                padding: UIEdgeInsets = .zero) {
        leadingAnchor.constraint(equalTo: left, constant: padding.left).isActive = true
        centerYAnchor.constraint(equalTo: centerY).isActive = true
        widthAnchor.constraint(equalToConstant: width).isActive = true
        heightAnchor.constraint(equalToConstant: height).isActive = true
        
        
    }
}

