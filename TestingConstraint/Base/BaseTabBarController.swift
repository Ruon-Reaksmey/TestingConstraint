//
//  BaseTabBarController.swift
//  TestingConstraint
//
//  Created by Reaksmey on 5/2/23.
//

import UIKit

class BaseTabBarController: UITabBarController,
                            UITabBarControllerDelegate {
    public var enableTabBarSeparateLine: Bool {
        true
    }
    
    public var customTabBarItemSize: CGSize? {
        nil
    }
    
    public var tabBarBorderColor: UIColor {
        Colors.primary500
    }
    
    public var tabBarBoarderWidth: CGFloat {
        1.0
    }
    
    public var tabBarBackgroundColor: UIColor {
        Colors.primary200
    }
    public var tabBarTintColor: UIColor {
        Colors.primary200
    }
    
    public var selectedItemTextAttribute: [NSAttributedString.Key: Any]? {
        nil
    }
    public var unSelectedItemTextAttribute: [NSAttributedString.Key: Any]? {
        nil
    }
    
    public var unselectedItemTintColor: UIColor {
        .systemGray2
    }
    
    public var selectedItemTintColor: UIColor {
        .black
    }
    
    // MARK: -Render Tab bar item base on tile 'unselectedItemTintColor' and 'unselectedItemTintColor'
    public var tabBarIconRenderingMode: UIImage.RenderingMode {
        .alwaysOriginal
    }
    
    private func reloadTabBarTitleAttribute() {
        tabBar.items?.forEach({ tab in
            if tab == tabBar.selectedItem {
                selectedItemTextAttribute.unwrap({
                    tab.setTitleTextAttributes($0, for: .normal)
                })
            } else {
                unSelectedItemTextAttribute.unwrap({
                    tab.setTitleTextAttributes($0, for: .normal)
                })
            }
        })
    }
    
    
    public var tabBars: [CustomTabBarItem] {
        []
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        prepareTabBars()
        initializeUI()
    }
    
    private func prepareTabBars() {
        var navs = [BaseNavigationController]()
        for tab in tabBars {
            let viewController = tab.viewController
            viewController.tabBarItem = tab.getTabBarItem(with: tabBarIconRenderingMode,
                                                          customSize: customTabBarItemSize)
            let nav = BaseNavigationController(rootViewController: viewController)
            navs.append(nav)
        }
        
        viewControllers = navs
    }
    
    private func initializeUI() {
        tabBar.unselectedItemTintColor = unselectedItemTintColor
        tabBar.tintColor               = selectedItemTintColor
        tabBar.clipsToBounds           = true
        tabBar.itemPositioning         = .fill
        tabBar.layer.borderWidth       = enableTabBarSeparateLine ? tabBarBoarderWidth : 0.0
        tabBar.layer.borderColor       = tabBarBorderColor.cgColor
        tabBar.isTranslucent = false
        tabBar.backgroundColor = tabBarBackgroundColor
        tabBar.barTintColor = tabBarTintColor
        reloadTabBarTitleAttribute()
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        reloadTabBarTitleAttribute()
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        true
    }
    
    
    
}


