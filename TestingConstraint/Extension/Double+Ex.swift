//
//  Double+Ex.swift
//  TestingConstraint
//
//  Created by Reaksmey on 5/2/23.
//

import Foundation

extension Double {
    
    public var toUTFormat: String {
        (toStringWithSpaceWithoutDecimal ?? "0") + " UT"
    }
    
    // MARK: - Default currency will be not rounding
    public var toDollarFormat: String {
        "$ " + toNumberFormat
    }
    
    public var toNumberFormat: String {
        toStringWithSpaceAndTrimDecimal ?? "0"
    }
    
    public var toNumberFormatWithoutRounding: String {
        cutOffDecimalsAfter(GeneralConstant.maxDecimalPlace).toStringWithSpaceAndTrimDecimal ?? "0"
    }
    
    public var toDollarFormatWithRounding: String {
        let formated = toStringWithSpaceAndTrimDecimal ?? "0"
        return "$ " + formated
    }
    
    public var isNotZero: Bool {
        !isZero
    }
    
    public var isNegative: Bool {
        self < .zero
    }
    
    public var isGreterThanZero: Bool {
        self > .zero
    }
    
    func roundTo(places: Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
    
    func cutOffDecimalsAfter(_ places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self*divisor).rounded(.towardZero) / divisor
    }
    
    func removeZerosFromEnd() -> String {
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "en_US")
        let number = NSNumber(value: self)
        formatter.numberStyle = .decimal
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = (self.toString.components(separatedBy: ".").last)!.count
        return String(formatter.string(from: number) ?? "")
    }
    
    func toUSCurrentcy(minimumFractionDigits: Int = 2, maximumFractionDigits: Int = 2) -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .currency
        numberFormatter.locale = Locale(identifier: "en_US")
        numberFormatter.minimumFractionDigits = minimumFractionDigits
        numberFormatter.maximumFractionDigits = maximumFractionDigits
        return numberFormatter.string(from: NSNumber(value: self))!
    }
    
    var toCompactShort: String {
        var tralingSymbol: String = "",
            devidedValue = self
        
        switch self {
        case 1000..<1000000:
            devidedValue = (self / 1000)
            tralingSymbol = "K"

        case 1000000... :
            devidedValue = (self / 1000000)
            tralingSymbol = "M"
            
        default: ()
        }

        return devidedValue.toString + tralingSymbol
    }
    
    var toString: String { "\(self)" }
    
    var toStringWithComma: String? {
        let numberFormatter = DoubleWithCommaFormatter()
        numberFormatter.minimumFractionDigits = 1
        numberFormatter.maximumFractionDigits = 1
        numberFormatter.locale = Locale(identifier: "en_US")
        return numberFormatter.string(for: self)
    }
    
    var toStringWithSpace: String? {
        let numberFormatter = DoubleWithSpaceFormatter()
        numberFormatter.minimumFractionDigits = 2
        numberFormatter.maximumFractionDigits = 2
        numberFormatter.locale = Locale(identifier: "en_US")
        return numberFormatter.string(for: self)
    }
    
    var toStringWithSpaceWithoutDecimal: String? {
        let numberFormatter = DoubleWithSpaceFormatter()
        numberFormatter.minimumFractionDigits = 0
        numberFormatter.maximumFractionDigits = 0
        numberFormatter.locale = Locale(identifier: "en_US")
        return numberFormatter.string(for: self)
    }
    
    var toStringWithSpaceAndTrimDecimal: String? {
        let decimalCount = (self.toString.components(separatedBy: ".").last)!.count
        let numberFormatter = DoubleWithSpaceFormatter()
        numberFormatter.minimumFractionDigits = 0
        numberFormatter.maximumFractionDigits = decimalCount > 2 ? 2 : decimalCount
        numberFormatter.locale = Locale(identifier: "en_US")
        return numberFormatter.string(for: self)
    }
    
    var toInt: Int {
        get {
            return Int.init(self)
        }
    }
    
    var toSquareMeters: String {
        let formatter = MeasurementFormatter()
        formatter.locale = Locale(identifier: "en_US")
        formatter.numberFormatter.minimumFractionDigits = 1
        return formatter.string(from: Measurement.init(value: self, unit: UnitArea.squareMeters)).description
    }
    
    func toSquareMeters(_ maximumFractionDigits: Int = 1) -> String {
        let formatter = MeasurementFormatter()
        formatter.locale = Locale(identifier: "en_US")
        formatter.numberFormatter.minimumFractionDigits = maximumFractionDigits
        return formatter.string(from: Measurement.init(value: self, unit: UnitArea.squareMeters)).description
    }
    
    var toMeters: String {
        let formatter = MeasurementFormatter()
        formatter.locale = Locale(identifier: "en_US")
        formatter.numberFormatter.minimumFractionDigits = 1
        return formatter.string(from: Measurement.init(value: self, unit: UnitLength.meters)).description
    }
    
    var toKilometers: String {
        let formatter = MeasurementFormatter()
        formatter.locale = Locale(identifier: "en_JP")
        return formatter.string(from: Measurement.init(value: self, unit: UnitLength.kilometers)).description
    }
    
    var metersToKilometers: String {
        let convertedKm = Measurement(value: self / 1000, unit: UnitLength.kilometers),
            formatter = MeasurementFormatter()
        
        formatter.locale = Locale(identifier: "en_JP")
        formatter.numberFormatter.maximumFractionDigits = 2
        return formatter.string(from: convertedKm).description
    }
}

class DoubleWithCommaFormatter: NumberFormatter {
    override init() {
        super.init()
        numberStyle = .decimal
        usesGroupingSeparator = true
        groupingSeparator = ","
        roundingMode = .halfEven
        groupingSize = 3
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class DoubleWithSpaceFormatter: NumberFormatter {
    override init() {
        super.init()
        numberStyle = .decimal
        usesGroupingSeparator = true
        groupingSeparator = " "
        roundingMode = .halfEven
        groupingSize = 3
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
