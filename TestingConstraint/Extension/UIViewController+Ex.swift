//
//  UIViewController+Ex.swift
//  TestingConstraint
//
//  Created by Reaksmey on 5/2/23.
//

import UIKit
import AVFoundation

extension UIViewController {
    
    public func popBackViewController(animated: Bool = true,
                                      isRightToLeft: Bool = false,
                                      completionHandler: VoidClosure? = nil) {
        if isRightToLeft {
            let transition = CATransition()
            transition.duration = 0.28
            transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeIn)
            transition.type = .push
            transition.subtype = .fromRight
            navigationController?.view.layer.add(transition, forKey: kCATransition)
            navigationController?.popViewController(animated: false)
        } else {
            navigationController?.popViewController(animated: animated)
        }
        completionHandler?()
    }
    
    public func popToRootViewController(animated: Bool = true) {
        navigationController?.popToRootViewController(animated: animated)
    }
    
    func forceLoadViewHierachy() {
        let _ = view
    }
    
    var navigationHeight: CGFloat {
        get {
            let navH = navigationController?.navigationBar.frame.height ?? 44.00
            return navH + statusBarHeight
        }
    }
    
    var statusBarHeight: CGFloat {
        var statusBarHeight: CGFloat = 0
        if #available(iOS 13.0, *) {
            let window = UIApplication
                .shared
                .windows
                .filter {$0.isKeyWindow}
                .first
            statusBarHeight = window?
                .windowScene?
                .statusBarManager?
                .statusBarFrame
                .height ?? 0
        } else {
            statusBarHeight = UIApplication.shared.statusBarFrame.height
        }
        return statusBarHeight
    }
    
    var screenWidth: CGFloat {
        return self.view.bounds.width
    }
    
    var screenHeight: CGFloat {
        return self.view.bounds.height
    }
    
    var isNoneBottomSafeArea: Bool {
        safeAreaHeight.bottom.isZero
    }
    
    var dynamicBottomSafeAreaMargin: CGFloat {
        isNoneBottomSafeArea ? 24.0 : .zero
    }
    
    var safeAreaHeight: (top: CGFloat, bottom: CGFloat) {
        if #available(iOS 13.0, *) {
            let window = UIApplication
                .shared
                .windows
                .filter {$0.isKeyWindow}
                .first
            let topPadding = window?.safeAreaInsets.top ?? 0
            let bottomPadding = window?.safeAreaInsets.bottom ?? 0
            return (topPadding, bottomPadding)
        } else {
            let window = UIApplication.shared.keyWindow
            let topPadding = window?.safeAreaInsets.top ?? 0
            let bottomPadding = window?.safeAreaInsets.bottom ?? 0
            return (topPadding, bottomPadding)
        }
    }
    
    var tabarHeight: CGFloat {
        get {
            self.tabBarController?.tabBar.frame.size.height ?? 0
        }
    }
    
    var rootViewController: UIViewController? {
        return (self as? UINavigationController)?.viewControllers.first
    }
    
    var isModal: Bool {
        if let index = navigationController?.viewControllers.firstIndex(of: self), index > 0 {
            return false
        } else if presentingViewController != nil {
            return true
        } else if let navigationController = navigationController, navigationController.presentingViewController?.presentedViewController == navigationController {
            return true
        } else if let tabBarController = tabBarController, tabBarController.presentingViewController is UITabBarController {
            return true
        } else {
            return false
        }
    }
    
    @objc func authorizedUser() {}
    
    
    @objc func back(_ completion: (()->Void)? = nil){
        if isModal {
            dismiss(animated: true, completion: completion)
        } else {
            navigationController?.popViewController(animated: true)
        }
    }
    
    @objc func push(to vc: UIViewController,
                    animated: Bool = true,
                    isLeftToRight: Bool = false,
                    hideTabBar: Bool = true,
                    completion: (()->Void)? = nil) {
        vc.hidesBottomBarWhenPushed = hideTabBar
        if isLeftToRight {
            let transition = CATransition()
            transition.duration = 0.28
            transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeIn)
            transition.type = .push
            transition.subtype = .fromLeft
            navigationController?.view.layer.add(transition, forKey: kCATransition)
            navigationController?.pushViewController(vc, animated: false)
        } else {
            navigationController?.pushViewController(vc, animated: animated)
        }
        completion?()
    }
    
    @objc func presentFullScreen(vc: UIViewController, animated: Bool = true, wrapWithNavigation: Bool = true, completion: (()->Void)? = nil) {
        if wrapWithNavigation {
            let navigation = BaseNavigationController(rootViewController: vc)
            navigation.modalTransitionStyle = .crossDissolve
            navigation.modalPresentationStyle = .fullScreen
            present(navigation, animated: animated, completion: completion)
        } else {
            let vc = vc
            vc.modalTransitionStyle = .crossDissolve
            vc.modalPresentationStyle = .overFullScreen
            present(vc, animated: true)
        }
    }
    
    public func presentCustomBottomSheet(_ bottomSheetViewController: UIViewController, completion: (()->Void)? = nil) {
        bottomSheetViewController.modalPresentationStyle = .overFullScreen
        present(bottomSheetViewController, animated: false, completion: completion)
    }
    
}
