//
//  Optional+Ex.swift
//  TestingConstraint
//
//  Created by Reaksmey on 5/2/23.
//

import Foundation

extension Optional where Wrapped == String {
    
    var isNilOrEmpty: Bool {
        return self == "" || self == nil
    }
}

extension Optional where Wrapped: RangeReplaceableCollection {
    
    /// Append element to array althought the array is null.
    public mutating func forceAppend(element: Wrapped.Element) {
        self = (self ?? .init()) + [element]
    }
    
    /// Append elements to array althought the array is null.
    public mutating func forceAppend(contentOf elements: [Wrapped.Element]) {
        self = (self ?? .init()) + elements
    }
}

extension Optional {
    
    /// return true if your optional is Nil
    public var isNull: Bool {
        return self == nil
    }
    
    /// return true if your optional is Not Nil
    public var isNotNull: Bool {
        return !isNull
    }
    
    /// easier unwrrap optional without if let or guard let
    public func unwrap( _ execute: ((_ unwrappedValue: Wrapped)->Void)?, nullHandler: (()->Void)? = nil ) {
        if let value = self {
            execute?(value)
        } else {
            nullHandler?()
        }
    }
}
