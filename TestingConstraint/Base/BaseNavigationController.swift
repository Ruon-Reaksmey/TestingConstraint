//
//  BaseNavigationController.swift
//  TestingConstraint
//
//  Created by Reaksmey on 5/2/23.
//

import UIKit

class BaseNavigationController: UINavigationController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return statusBarStyle
    }
    
    var statusBarStyle: UIStatusBarStyle = .default {
        didSet {
            setNeedsStatusBarAppearanceUpdate()
            print(";hi")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
