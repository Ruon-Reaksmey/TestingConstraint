//
//  RootIconNames.swift
//  TestingConstraint
//
//  Created by Reaksmey on 5/2/23.
//

import UIKit

enum RootIconNames: String, ImageLoadable {
    case iconHome
    case iconPromotion
    case iconNotification
    case iconContactUs
}
