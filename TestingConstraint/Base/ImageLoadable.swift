//
//  ImageLoadable.swift
//  TestingConstraint
//
//  Created by Reaksmey on 5/2/23.
//

import UIKit

protocol ImageLoadable where Self: RawRepresentable {}

extension ImageLoadable {
    
    public func getIcon(resized: CGSize? = nil) -> UIImage? {
        if let raw = rawValue as? String {
            return resized.isNull ? UIImage(named: raw) : UIImage(named: raw)?.reSizeImage(reSize: resized!)
        }
        return nil
    }
}
