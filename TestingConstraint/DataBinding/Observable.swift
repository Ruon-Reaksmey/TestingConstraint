//
//  Observable.swift
//  TestingConstraint
//
//  Created by Reaksmey on 5/2/23.
//

import Foundation

public typealias VoidClosure = ()->Void
public typealias VoidClosureParam<T> = (T)->Void


typealias Obs = Observable

class Observable<T> {
    typealias Listener = (T) -> Void
    
    private var shouldNotify = true
    
    var listener: Listener?
    
    var value: T {
        didSet {
            if shouldNotify {
                notify()
            }
            shouldNotify = true
        }
    }
    
    var silentValue: T {
        set {
            shouldNotify = false
            value = newValue
        }
        
        get {
            value
        }
    }
    
    init(_ value: T, notify: Bool = true) {
        shouldNotify = notify
        self.value = value
    }
    
    func observe(listener: Listener?) {
        self.listener = listener
        listener?(value)
    }
    
    func notify() {
        listener?(value)
    }
}
