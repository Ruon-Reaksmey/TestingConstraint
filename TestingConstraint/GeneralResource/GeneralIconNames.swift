//
//  GeneralIconNames.swift
//  TestingConstraint
//
//  Created by Reaksmey on 6/2/23.
//

import UIKit

enum GeneralIconNames: String, ImageLoadable {
    case iconBack
    case iconCross
    case iconMenuOption
    case imagePlaceholder
}
