//
//  Colors.swift
//  TestingConstraint
//
//  Created by Reaksmey on 5/2/23.
//

import UIKit

struct Colors {
    
    // MARK: -Primary
    static var primary50        = UIColor(light: UIColor("7782BF"), dark: UIColor("7782BF"))
    static var primary100       = UIColor(light: UIColor("4A5193"), dark: UIColor("4A5193"))
    static var primary200       = UIColor(light: UIColor("2C2F6A"), dark: UIColor("2C2F6A"))
    static var primary500       = UIColor(light: UIColor("1B2266"), dark: UIColor("1B2266"))

    // MARK: -Secondary
    static var secondary50      = UIColor(light: UIColor("FFEAC0"), dark: UIColor("FFEAC0"))
    static var secondary100     = UIColor(light: UIColor("FFDA88"), dark: UIColor("FFDA88"))
    static var secondary200     = UIColor(light: UIColor("F4C142"), dark: UIColor("F4C142"))
    static var secondary500     = UIColor(light: UIColor("E0AC19"), dark: UIColor("E0AC19"))

    // MARK: -GrayScale
    static var gray50           = UIColor(light: UIColor("F5F5FA"), dark: UIColor("F5F5FA"))
    static var gray100          = UIColor(light: UIColor("E4E5F1"), dark: UIColor("E4E5F1"))
    static var gray200          = UIColor(light: UIColor("CFD0E2"), dark: UIColor("CFD0E2"))
    static var gray300          = UIColor(light: UIColor("B3B5CC"), dark: UIColor("B3B5CC"))
    static var gray400          = UIColor(light: UIColor("9496B8"), dark: UIColor("9496B8"))
    static var gray500          = UIColor(light: UIColor("6C6F93"), dark: UIColor("6C6F93"))
    static var gray600          = UIColor(light: UIColor("47496B"), dark: UIColor("47496B"))
    static var gray700          = UIColor(light: UIColor("32344B"), dark: UIColor("32344B"))
    static var gray800          = UIColor(light: UIColor("1D1E30"), dark: UIColor("1D1E30"))
    static var gray900          = UIColor(light: UIColor("0A0B12"), dark: UIColor("0A0B12"))
    static var grayDDD          = UIColor(light: UIColor("DDDDDD"), dark: UIColor("DDDDDD"))
    static var gray888          = UIColor(light: UIColor("888888"), dark: UIColor("888888"))
    static var gray666          = UIColor(light: UIColor("666666"), dark: UIColor("666666"))
    static var lighGrayEEE      = UIColor(light: UIColor("EEEEEE"), dark: UIColor("EEEEEE"))
    static var grayBG           = UIColor(light: UIColor("F9F9F9"), dark: UIColor("F9F9F9"))
    static var grayE6E6         = UIColor(light: UIColor("E6E6E6"), dark: UIColor("E6E6E6"))
    static var lighGrayD9D9      = UIColor(light: UIColor("D9D9D9"), dark: UIColor("D9D9D9"))
    static var grayE9E      = UIColor(light: UIColor("9E9E9E"), dark: UIColor("9E9E9E"))


    // MARK: -Other
    static var primaryGray      = primary500.withAlphaComponent(40)
    static var positive         = UIColor(light: UIColor("068453"), dark: UIColor("068453"))
    static var negative         = UIColor(light: UIColor("DD3636"), dark: UIColor("DD3636"))
    static var warning          = UIColor(light: UIColor("DF8620"), dark: UIColor("DF8620"))
    static var tabBarUnselected = UIColor(light: UIColor("666666"), dark: UIColor("666666"))
    static var bannerDotColor   = UIColor(light: UIColor("888888"), dark: UIColor("888888")).withAlphaComponent(0.5)
    static var lightGreen       = UIColor("08B471")

    static var lineChartTextColor       = UIColor(light: UIColor("707070"), dark: UIColor("707070"))
//    static var pieChartTradingBalance   = UIColor(light: UIColor("194DE0"), dark: UIColor("194DE0"))
//    static var pieChartUTProject        = UIColor(light: UIColor("E3C82F"), dark: UIColor("E3C82F"))
    static var disabled                 = UIColor(light: UIColor("888888"), dark: UIColor("888888"))
    static var segmentControlGray       = UIColor(light: UIColor("F7F7F7"), dark: UIColor("F7F7F7"))
    static var separateLine             = UIColor(light: UIColor("0A0B12"), dark: UIColor("0A0B12")).withAlphaComponent(0.1)
    
    // MARK: -Blue
    static var blueC6B          = UIColor(light: UIColor("0E1C6B"), dark: UIColor("0E1C6B"))
    static var blue872          = UIColor(light: UIColor("1A2872"), dark: UIColor("1A2872"))
    
    // MARK: -Blue
    static var whiteFFF          = UIColor(light: UIColor("FFFFFF"), dark: UIColor("FFFFFF"))


    // MARK: -Red
    static var red757          = UIColor(light: UIColor("E25757"), dark: UIColor("E25757"))
    
    // MARK - AlphaBlue
    static var blueAlpha       = UIColor(red: 0.05, green: 0.45, blue: 0.90, alpha: 0.05)
}
