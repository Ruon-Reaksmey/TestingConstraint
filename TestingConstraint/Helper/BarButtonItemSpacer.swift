//
//  BarButtonItemSpacer.swift
//  TestingConstraint
//
//  Created by Reaksmey on 6/2/23.
//

import UIKit

class BarButtonItemSpacer: UIBarButtonItem {

    convenience init(_ space: CGFloat) {
        self.init(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        self.width = space
    }
}
