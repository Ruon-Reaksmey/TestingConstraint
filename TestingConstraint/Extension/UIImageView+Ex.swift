//
//  UIImageView+Ex.swift
//  TestingConstraint
//
//  Created by Reaksmey on 6/2/23.
//

import UIKit
import SDWebImage

extension UIImageView {
    
    func imageFromURLString(_ url: String?, placeholder: UIImage? = GeneralIconNames.imagePlaceholder.getIcon()) {
        if let url = url {
            sd_setImage(with: URL(string: url), placeholderImage: placeholder)
        }
    }
    
    func makeRoundedImage(percent: CGFloat = 0.25) {
        let maskLayer = CAShapeLayer(layer: self.layer)
        let bezierPath = UIBezierPath()
        
        bezierPath.move(to: CGPoint(x:0, y:0))
        bezierPath.addLine(to: CGPoint(x:self.bounds.size.width, y:0))
        bezierPath.addLine(to: CGPoint(x:self.bounds.size.width, y:self.bounds.size.height - (self.bounds.size.height*0.25)))
        bezierPath.addQuadCurve(to: CGPoint(x:0, y:self.bounds.size.height - (self.bounds.size.height*0.25)), controlPoint: CGPoint(x:self.bounds.size.width/2, y:self.bounds.size.height))
        bezierPath.addLine(to: CGPoint(x:0, y:0))
        
        maskLayer.path = bezierPath.cgPath
        maskLayer.frame = self.bounds
        maskLayer.masksToBounds = true
        self.layer.mask = maskLayer
    }
}

extension UIImageView {
    
    func addCoverLayer(bgColor: CGColor, radius: CGFloat, opacity: Float, removeSub: Bool = true) {
        if removeSub {
            self.layer.sublayers = nil
        }
        let coverLayer3 = CALayer()
        coverLayer3.frame = self.bounds
        coverLayer3.backgroundColor = bgColor
        coverLayer3.cornerRadius = radius
        coverLayer3.opacity = opacity
        self.layer.addSublayer(coverLayer3)
        self.layer.borderColor = Colors.gray888.cgColor
    }
    
}
