//
//  HomeTableViewCell.swift
//  TestingConstraint
//
//  Created by Reaksmey on 5/2/23.
//

import UIKit

class HomeTableViewCell: UITableViewCell {

    static let identifier = "HomeTableViewCell"
    let label: UILabel = {
        let label = UILabel()
        label.text = "ACLEDA BANK"
        label.font = .systemFont(ofSize: 24)
        return label
    }()
    
    let imageAcleda: UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "iconAcleda")
        image.contentMode = .scaleAspectFit
        return image
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(label)
        contentView.addSubview(imageAcleda)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.anchor(left: contentView.leftAnchor,
                     right: contentView.rightAnchor, top: contentView.topAnchor,
                     bottom: contentView.bottomAnchor,
                     padding: .zero)
        
        imageAcleda.translatesAutoresizingMaskIntoConstraints = false
        imageAcleda.anchor(left: contentView.leadingAnchor,
                           centerY: contentView.centerYAnchor,
                           width: 75,
                           height: 75,
                           padding: .init(top: 0,
                                          left: 200,
                                          bottom: 0,
                                          right: 0))
    }
}
